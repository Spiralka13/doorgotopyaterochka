//
//  ChatController.swift
//  doOrGoToPyaterochka
//
//  Created by  Евгений on 18/11/2018.
//  Copyright © 2018 LosAnatoly Inc. All rights reserved.
//

import UIKit

class ChatController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    
    //TODO: - Зарегать второй ксиб ответа и реализовать дальнейшую логику работы
    
    private let identifier = String(describing: InputMessageCell.self)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        tableView.backgroundView = UIImageView(image: UIImage(named: "wall"))
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    func setupViews() {
        
        sendButton.layer.cornerRadius = 19
        sendButton.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        sendButton.layer.masksToBounds = true
        sendButton.setImage(UIImage(named: "placeholder"), for: .normal)
        
    }
    
    @IBAction func lolkekPressed(_ sender: Any) {
        UIView.transition(with: sendButton, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.sendButton.setImage(nil, for: .normal)
            
        }, completion: nil)
        
  
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                self.sendButton.bounds.size.width += 30
                self.sendButton.bounds.size.height += 30
            })
         UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.5, animations: {
            self.sendButton.bounds.size.width -= 30
            self.sendButton.bounds.size.height -= 30
        })
        }, completion: nil)
    }
}

extension ChatController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? InputMessageCell
        return cell ?? UITableViewCell()
    }
    
    
}


extension ChatController: UITableViewDelegate {
    
}
